
const char help[] = "Example program for steric repulsion on GPUs.\n";

#include <curand.h>
#include <curand_kernel.h>
#include <petscsys.h>

/* These are constants, but they are not compiled in */
__constant__ PetscInt  dim;
__constant__ PetscInt  numParticles;
__constant__ PetscReal M;
__constant__ PetscReal a;
__constant__ PetscReal krepul;
__constant__ PetscReal L;
__constant__ PetscReal dt;
__constant__ PetscReal sqrt2dt;
__device__   float     globalAvg;

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error %s", cudaGetErrorString(_cerr));} while(0)

/* Initialize a random number generator */
__global__ void setup_kernel(curandState_t *randState, size_t numStates)
{
  int id       = threadIdx.x + blockIdx.x * blockDim.x;
  int gridSize = blockDim.x * gridDim.x;
  int i;

  for (i = id; i < numStates; i+= gridSize) {
    /* Each variable gets same seed, a different sequence number, no offset */
    curand_init(0, i, 0, &randState[i]);
  }
}

/* Set initial random locations of particles */
__global__ void initialize_points(PetscReal *x, curandState_t *randState)
{
  int id       = threadIdx.x + blockIdx.x * blockDim.x;
  int gridSize = blockDim.x * gridDim.x;
  int i;

  for (i = id; i < dim * numParticles; i += gridSize) {
    x[i] = (PetscReal) L * (curand_uniform_double(&randState[i]) - 0.5);
  }
}

/* Add the forces and random noise to the position */
__global__ void sum_noise_and_forces(PetscReal *x, PetscReal *forces, curandState_t *randState)
{
  int id       = threadIdx.x + blockIdx.x * blockDim.x;
  int gridSize = blockDim.x * gridDim.x;
  int i;

  for (i = id; i < dim * numParticles; i += gridSize) {
    x[i] += forces[i] * M * dt + sqrt2dt * curand_normal_double(&randState[i]);
  }
}

/* Compute the forces between two particles:
   - one thread to update each particle */
__global__ void compute_forces(PetscReal *x, PetscReal *forces)
{
  int id            = threadIdx.x + blockIdx.x * blockDim.x;
  int gridSize      = blockDim.x * gridDim.x;
  int i, j, k;

  for (i = id; i < numParticles; i += gridSize) {
    for (j = 0; j < numParticles; j++) {
      double dist2 = 0.;

      if (i == j) continue;
      for (k = 0; k < dim; k++) {
        double disp = remainder(x[dim * i + k] - x[dim * j + k],L);

        dist2 += disp * disp;
      }
      if (dist2 < 4. * a * a) {
        double dist = sqrt(dist2);
        double f = krepul * (2. - dist);

        for (k = 0; k < dim; k++) {
          double disp = remainder(x[dim * i + k] - x[dim * j + k],L);

          forces[dim * i + k] += f * disp / dist;
        }
      }
    }
  }
}

/* Get the average distance traveled */
__global__ void average_distance(PetscReal *x, PetscReal *xInit)
{
  int               lid           = threadIdx.x;
  int               id            = threadIdx.x + blockIdx.x * blockDim.x;
  int               gridSize      = blockDim.x * gridDim.x;
  __shared__ double *avg;
  int               i, j, s;

  if (!lid) {
    avg = (double *) malloc(2. * blockDim.x * sizeof(double));
  }
  __syncthreads();
  avg[lid] = 0.;
  avg[blockDim.x + lid] = 0.;
  for (i = id; i < numParticles; i += gridSize) {
    double dist = 0.;

    for (j = 0; j < dim; j++) {
      double disp;

      disp = x[dim * i + j] - xInit[dim * i + j];
      dist += disp * disp;
    }
    avg[lid] += sqrt(dist) / numParticles;
  }
  /* thread reduce */
  for (s = blockDim.x / 2; s > 0; s >>= 1)
  {
    if (s >= 32) {
      __syncthreads();
    }
    avg[lid] += avg[lid + s];
  }
  if (!lid) {
    (void) atomicAdd(&globalAvg, (float) avg[0]);
    free(avg);
  }
}

int main(int argc, char **argv)
{
  MPI_Comm              comm;
  PetscInt              h_numParticles = 10000, h_dim = 3, numSteps = 1000, i;
  PetscReal             h_L = 68., h_dt = 1.e-4, h_M = 1., h_a = 1., h_krepul = 100., h_sqrt2dt;
  PetscMPIInt           size;
  PetscErrorCode        ierr;
  PetscReal            *x, *xInit, *forces;
  cudaError_t           cerr;
  curandState_t        *randState;
  int                   block, grid;
  float                 h_globalAvg;
  struct cudaDeviceProp prop;


  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  if (size != 1) SETERRQ(comm, PETSC_ERR_ARG_OUTOFRANGE, "This is a serial MPI program, run with one process\n");

  ierr = PetscOptionsBegin(comm, NULL, "Exercise 6 Options", "ex06.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_particles", "Number of particles in test", "ex06.c", h_numParticles, &h_numParticles, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-dim", "Number of dimensions", "ex06.c", h_dim, &h_dim, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-L", "Width of the domain [-L/2,L/2) in each direction", "ex06.c", h_L, &h_L, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-dt", "Time step", "ex06.c", h_dt, &h_dt, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-k_repul", "Repulsion constant", "ex06.c", h_krepul, &h_krepul, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_steps", "Number of time steps", "ex06.c", numSteps, &numSteps, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscPrintf(comm, "Testing %D particles in %D dimensions with steric repulsion:\n", h_numParticles, h_dim);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "  particle mass:   %g\n", (double) h_M); CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "  particle radius: %g\n", (double) h_a); CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "  k_repulsion:     %g\n", (double) h_krepul); CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "  box length:      %g\n", (double) h_L); CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "  time step:       %g\n", (double) h_dt); CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "  number of steps: %g\n", (double) numSteps); CHKERRQ(ierr);

  /* Copy constants to the device __constant__ memory */
  cerr = cudaMemcpyToSymbol(dim, &h_dim, sizeof(PetscInt)); CUDA_CHK(cerr);
  cerr = cudaMemcpyToSymbol(numParticles, &h_numParticles, sizeof(PetscInt)); CUDA_CHK(cerr);
  cerr = cudaMemcpyToSymbol(M, &h_M, sizeof(PetscReal)); CUDA_CHK(cerr);
  cerr = cudaMemcpyToSymbol(a, &h_a, sizeof(PetscReal)); CUDA_CHK(cerr);
  cerr = cudaMemcpyToSymbol(krepul, &h_krepul, sizeof(PetscReal)); CUDA_CHK(cerr);
  cerr = cudaMemcpyToSymbol(L, &h_L, sizeof(PetscReal)); CUDA_CHK(cerr);
  cerr = cudaMemcpyToSymbol(dt, &h_dt, sizeof(PetscReal)); CUDA_CHK(cerr);
  h_sqrt2dt = PetscSqrtReal(2. * h_dt);
  cerr = cudaMemcpyToSymbol(sqrt2dt, &h_sqrt2dt, sizeof(PetscReal)); CUDA_CHK(cerr);

  /* Create the vectors of positions and forces */
  cerr = cudaMalloc(&x, h_dim * h_numParticles * sizeof(*x)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&xInit, h_dim * h_numParticles * sizeof(*xInit)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&forces, h_dim * h_numParticles * sizeof(*xInit)); CUDA_CHK(cerr);

  /* Create random number generators */
  cerr = cudaMalloc(&randState, h_dim * h_numParticles * sizeof(*randState)); CUDA_CHK(cerr);

  cerr = cudaGetDeviceProperties(&prop, 0); CUDA_CHK(cerr);
  block = prop.maxThreadsPerBlock;
  grid  = (h_dim * h_numParticles + block - 1) / block;

  setup_kernel<<<grid, block>>>(randState, h_dim * h_numParticles);
  initialize_points<<<grid, block>>>(x, randState);
  cerr = cudaMemcpy(xInit, x, h_dim * h_numParticles * sizeof(PetscReal), cudaMemcpyDeviceToDevice); CUDA_CHK(cerr);

  for (i = 0; i < numSteps; i++) {
    if (!(i % 1000)) {
      h_globalAvg = 0.;
      cerr = cudaMemcpyToSymbol(globalAvg, &h_globalAvg, sizeof(float)); CUDA_CHK(cerr);
      average_distance<<<grid, block>>>(x, xInit);
      cerr = cudaDeviceSynchronize(); CUDA_CHK(cerr);
      cerr = cudaMemcpyFromSymbol(&h_globalAvg, globalAvg, sizeof(float)); CUDA_CHK(cerr);
      ierr = PetscPrintf(comm, "Average distance traveled at time %g: %g\n", i * h_dt, (double) h_globalAvg); CHKERRQ(ierr);
    }
    cerr = cudaMemset(forces, 0, h_dim * h_numParticles * sizeof(PetscReal)); CUDA_CHK(cerr);
    compute_forces<<<grid, block>>>(x, forces);
    sum_noise_and_forces<<<grid, block>>>(x, forces, randState);
    cerr = cudaDeviceSynchronize(); CUDA_CHK(cerr);
  }
  if ((i % 1000) != 1) {
    h_globalAvg = 0.;
    cerr = cudaMemcpyToSymbol(globalAvg, &h_globalAvg, sizeof(float)); CUDA_CHK(cerr);
    average_distance<<<grid, block>>>(x, xInit);
    cerr = cudaDeviceSynchronize(); CUDA_CHK(cerr);
    cerr = cudaMemcpyFromSymbol(&h_globalAvg, globalAvg, sizeof(float)); CUDA_CHK(cerr);
    ierr = PetscPrintf(comm, "Average distance traveled at time %g: %g\n", i * h_dt, (double) h_globalAvg); CHKERRQ(ierr);
  }

  /* Destroy the random number generator contexts */
  cerr = cudaFree(randState); CUDA_CHK(cerr);
  /* Free the vectors */
  cerr = cudaFree(forces); CUDA_CHK(cerr);
  cerr = cudaFree(xInit); CUDA_CHK(cerr);
  cerr = cudaFree(x); CUDA_CHK(cerr);

  ierr = PetscFinalize();
  return ierr;
}
